package week1;

public class EightProgram {

        public static void main(String[] args) {
            int numberOfMonthsInAYear;
            int numberOfDaysInAMonth;
            String result;
            String[] nameOfMonths;
            nameOfMonths = new String[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
            int[] amountOfDays;
            amountOfDays = new int[] {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

            for (int i = 0, j = 0; i < nameOfMonths.length; i++)
            {
                j = (i + 1) / 3;
                if (j == 4) j = 0;
                System.out.println(nameOfMonths[i] + " " + amountOfDays[i]);
            }

        }
}
